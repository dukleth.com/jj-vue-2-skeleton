import Vue from 'vue';
import Vuex from 'vuex';
import templateStepperConfig from './modules/template/store';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    templateStepperConfig,
  },
});
