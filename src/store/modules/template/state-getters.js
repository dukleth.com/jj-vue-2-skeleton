export const state = {
  steps: [
    {
      id: 1,
      label: 'Step 1',
      component: 'template-step-form',
    },
    {
      id: 2,
      label: 'Step 2',
      component: 'template-step-form',
    },
    {
      id: 3,
      label: 'Step 3',
      component: 'template-step-form',
    },
  ],
};

export const getters = {
  // currentView: state => {
  //   return state.currentView;
  // },
};
