import Vue from 'vue';
import Router from 'vue-router';

// lazy load routes (instead of a static import)
const TemplateApplication = () => import('@/components/template-application/TemplateApplication');

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'TemplateApplication',
      component: TemplateApplication,
    },
  ],
});
