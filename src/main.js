// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Vuetify from 'vuetify';
import VeeValidate from 'vee-validate';
import propDoc from 'propdoc';
import '../node_modules/vuetify/dist/vuetify.min.css';

import router from './router';
import store from './store/store';
import App from './components/App';

Vue.config.productionTip = false;

Vue.use(Vuetify);
Vue.use(VeeValidate);
Vue.use(propDoc);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
});
